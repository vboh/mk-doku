﻿Střední průmyslová škola elektrotechnická
a Vyšší odborná škola Pardubice


STŘEDNÍ PRŮMYSLOVÁ ŠKOLA ELEKTROTECHNICKÁ








MATURITNÍ PRÁCE

Tvorba arkádové hry v Javě





Březen 2017		Milena Kliková




















„Prohlašuji, že jsem maturitní práci vypracoval(a) samostatně a použil(a) jsem literárních pramenů a informací, které cituji a uvádím v seznamu použité literatury a zdrojů informací.“

Pardubice dne ...........................	................................................
	podpis

 

 
 
Anotace
Tato práce má za cíl popsat vytvoření 2D arkádové hry v Java platformě s využitím přídavných knihoven LWJGL, MarteEngine, Slick a vestavěných knihoven Javy. Hra dostala název Luna’s Adnventure, protože sleduje dobrodružství obtloustlé kočky Luny, která se vydává na cestu za jídlem, aby neztratila své kulaté křivky, poté co jí její majitelka nařídí dietu. Hra je také výjimečná tím že využívá ručně malovanou grafiku, která byla poté upravována v programech Adobe Photoshop a RealDraw.
Klíčová slova:
Kočka, Engine, LWJGL, Java, třída, metoda, objekt












 
Annotation
This work has as a goal to describe creating of 2D arcade game made at Java platform with using additional libraries as LWJGL, MarteEngine, Slick and also build-in Java libraries. The game has got a name Luna’s Adventure because it tracks adventure of chubby cat named Luna, who is embarks on journey for food to not lost her round curves, after what her owner order her diet. The game is also interesting thanks to using handmade graphic, which was after adjust by Adobe Photoshop and RealDraw.
Keywords:
Cat, Engine, LWJGL, Java, class, method, object
 
Seznam použitých značek a symbolů



















Úvod
Pro vytvoření hry jsem se rozhodla na základě svého dětského snu. Již v útlém věku jsem měla zálibu v počítačových hrách a mým tajným přáním bylo vytvořit svou vlastní, originální a vtipnou hru. Ovšem vytvořit svou vlastní hru není jednoduché tak jsem se nikdy neodvážila jen pomyslet na realizaci něčeho tak pro mě neuvěřitelného. Poté ale přišel maturitní projekt a díky tomu se i naskytla dokonalá příležitost pro uskutečnění hry. 
Když už jsem se tedy rozhodla pro vytvoření, nastal problém o čem. Chtěla jsem vytvořit něco nekonvenčního a přinést trochu čerstvého vzduch do ohraných panáčků s meči co bojují proti příšerám. A tak se zrodil plán pro vytvoření Luna’s Adventure. Byla to dosti velká náhoda, ale jako inspirace mi posloužila kočka známého, která je poněkud oblejších tvarů. Vidět ji, jak se žene za jídlem, jako by to byl její smysl života, a pozorovat ji, jak zápasí s poličkou, na které byl pytel s granulemi, bylo osvícení, které jsem potřebovala.
Vytvořit hru v jazyce Java nebylo těžké rozhodnutí, jelikož se s jazykem Java učíme pracovat v 1. a 2. ročníku a chtěla jsem tyto vědomosti zúročit. Hru jsem programovala ve vývojovém prostředí NetBeans protože jsem na něj zvyklá a osobně na mě působí uspořádaně a přehledně. Dále jsem při vytváření hry pracovala s podpůrnými enginy jako např. Slick2D.








 
2.	Použité technologie
2.1.	Java
Programovací jazyk Java je jeden z nejrozšířenějších jazyků na světě. Je objektově orientovaný a nezávislý na platformě což znamená, že je spustitelný na jakékoliv platformě (Windows, Mac, Linux, ...). Jeho spustitelnost na jakémkoliv operačním systému je zajištěna způsobem kompilace. Většina jazyků překládá zdrojové kódy na strojový kód, ovšem Java si tvoří předem byte-kód který si poté překládá na strojový kód. Díky tomu může člověk přenášet byte-kód z počítače na počítač nezávisle na procesoru nebo operačním systému. Překlad z byte-kódu na strojový a opačně dělá tzv. JVM (Java Virtual Machine).
Další výhodou Javy je také to že je vyvíjena jako open source. Což znamená, že licence je volně přístupná při splnění jistých podmínek a že má volně přístupné zdrojové kódy.
2.2.	LWGLJ
LWJGL (Light Weight Java Game Library) je open-source knihovna určená pro herní vývojáře (jak začínající tak již zkušené). LWJGL také nahrazuje OpeGL (Open Graphic Library) za vlastní a podporuje OpenAL (Open Audio Library) a OpenCL (Open Computing Library).
2.3.	Slick2D
Slick2D je knihovna na bázi open-source která využívá některé funkce LWJGL a OpenGL. A má za funkci zjednodušit vývoj Java her. Přidává podporu animací, zvuků, hudby, efektů a obrázků. Jak už z názvu vyplívá je zaměřena na vývoj 2D her. Díky ní také můžeme využívat třídu StateBasedGame jsem velmi využila při vývoji hry.
2.4.	StateBaseGame
StateBasedGame je třída, která je součástí knihovny Slick2D. Její hlavní funkce je práce se stavy hry. Stav hry je oblast ve hře, která má za úkol pracovat se specifickým typem prostředí (Intro, Menu, Hra, ...). Všechny mé třídy využívající stavy, používají metody ze třídy StateBasedGame a díky tomu jsou schopné přepínat mezi stavy.
Hlavní funkce, které využívám:
\item[init] naplní deklarované proměnné hodnotami
\item[update] pracuje s entitami a objekty stavu a mění jejich vlastnosti v čase (např. kompletní pohyb hráče nebo interakce s bloky...)
\item[render] má za úkol pracovat s grafickými elementy hry (např. vykreslování pozadí či bloků)
2.5.	MarteEngine
MarteEngine je engine napsaný v jazyce Java, který se zaměřuje na API (rozhraní na vývoj aplikací) pro jednoduchý vývoj her. Obsahuje převážně funkce pro práci s entitami (např. detekce kolizí...).
2.6.	Entity
Třída Entity má na starost práci s herními objekty (entitami) mezi které patří herní pozadí, hráč, bloky... Nabízí několik předdefinovaných metod, které nám umožňují manipulovat s vlastnostmi daného herního objektu.
Hlavní funkce, které využívám:
\item[update] pokud bude entita existovat v aktivním stavu, bude se pravidelně v závislosti na času provádět daný kus programu
\item[render] vykresluje grafické objekty
2.7.	NetBeans IDE
NetBeans IDE je integrované vývojové prostředí na bázi open source. Vlastní ho firma Oracle Corporation, která také sponzoruje jeho vývoj. NetBeans je hlavně určeno pro vývoj a ladění aplikaci v programovacím jazyce Java, umožnuje ovšem i vývoj v jiných programovacích jazycích (např. Groovy, C++, HTML, PHP, ...).
NetBeans má velmi kvalitní systém „našeptávání“ což je také důvod proč jsem si ho zvolila. Editor pomáhá zvolit správné závorky, zvýrazňuje klíčová slova, odsazuje řádky ... Také nabízí řadu užitečných nástrojů, poskytuje šablony kódu a navíc díky své stavbě je spustitelný na různých platformách.
**
2.8.	Práce s akvarely
Grafiku hry jsem chtěla pojmout originálně a umělecky. Proto první, co mi přišlo na mysl, byla ručně malovaná grafika. Akvarely neboli vodovky byla skvělá volba. Díky vodě jsem dokázala udělat plynulé přechody a hrát si s intenzitou dané barvy. Abych docílila čistých obrysů, využila jsem také tuž a redispero. Tím jsme vše obtáhla a Luna’s Adventure získalo podobu aspoň na papíře.
Luna’s Adventure tak nabídne díky akvarelům autentický zážitek, který přenáší fyzickou tvorbu do virtuálního prostředí. Hra je navíc pestrobarevně zabarvena, z akvarelů jsem se proto snažila využít velikou škálu barev, aby hra působila „živým“ nádechem.
 
Obrázek 2: Ručně malované menu.
2.9.	Adobe Photoshop CS6
Photoshop je program na úpravu bitmapové grafiky (fotek, obrázků). Díky mnoha retušovacím  a ořezávacím nástrojů je ideální pro práci s již hotovou grafikou, která pouze potřebuje upravit, aby byla vhodná pro použití ve hře.
Verze CS 6 přináší radikální designovou proměnu. Nově přichází s tmavým skinem, který je nastaven jako výchozí, ale oslní i ostatními prvky, především těmi v uživatelském rozhraní. Ty byly totiž od minulých verzí výrazně přepracovány, aby odpovídaly moderním standardům i specifickým požadavkům profesionálních uživatelů, kteří chtěli přehledné a pevné ovládaní.
2.10.	BitmapCombine
Je primitivní program pro vytváření sprite sheetu. Sprite sheet je v mé hře důležitý, protože díky němu mohu plně využívat animace ve své hře, bez nich by to nešlo.
 
3.	O hře
Jde o dvourozměrnou arkádově žánrovou plošinovku, která se svou herní mechanikou inspiruje populární „skákačkou“ Super Mario.
Hra se jmenuje Luna’s Adventure a v překladu do češtiny to znamená Lunino Dobrodružství. Je to protože hra vypraví příběh kočky Luny, která se musí vypravit za jídlem. Její zlověstná majitelka jí totiž nařídila dietu, a to se Luně samozřejmě nelíbí. Proto se naše kočičí hrdina rozhodne získat jídlo vlastní silou. Vydává se tak na dlouho a nebezpečnou výpravu za jídlem, kde na ni čekají záludní nepřátelé jako například vysavač, gauč či sporák. Ti je brání, aby se dostala včas k jídlu.
Luna’s Adventure je inspirována reálnou skutečností. Její předloha opravdu existuje. Můj známý vlastní opravdu nemálo otylou kočku, pro kterou je smysl života právě jídlo. Jakmile jsem toho gigantické stvoření spatřila, hned jsem věděla, o čem bude má hra.
3.1.	Kočka
Kočka domácí (Felis silvestris f. catus) je domestikovaná forma kočky divoké, která je již po tisíciletí průvodcem člověka. Stejně jako její divoká příbuzná patří do podčeledi malé kočky, a je typickým zástupcem skupiny. Má pružné a svalnaté tělo, dokonale přizpůsobené lovu, ostré drápy a zuby a vynikající zrak, sluch a čich.
Kočka domácí vždy sloužila člověku především jako lovec hlodavců, v současnosti se uplatňuje také jako společník a mazlíček člověka. V některých oblastech Číny je konzumováno/konzervováno kočičí maso, ve třetím světě je kočka domácí rovněž kožešinovým zvířetem a výrobky z kočičí kožešiny se dostávaly i na evropský trh. V červnu 2007 byl však dovoz kočičí kůže a kožešiny do Evropy zakázán.
Člověk rozšířil kočku domácí prakticky na všech kontinentech (s výjimkou Antarktidy), na mnoha místech zpětně zdivočela.
 
Obrázek 3: Reálná předloha pro Lunu
3.2.	Intro
Intro je anglicismus a český slangový výraz pro úvodní část počítačové hry. Má za úkol hráče uvést do hry, představit mu jí nebo ho seznámit se základními souvislostmi hry. Nejprve šlo obvykle o počítačovou animaci, s rozvojem výpočetního výkonu počítačů ale některé hry přešly na scény renderované přímo pomocí herního enginu.
Vytvoření pěkné úvodní časti, intra, bylo pro mou hru opravdu klíčové, jelikož celý titul se opírá o zajímavý příběh a pro pochopení hry je důležité být v obraze. Právě od toho je zde intro.
 
Obrázek 4: Jeden z obrázků intra
3.3.	Ovládání hry
Ovládání hry je přizpůsobené pro klávesnici. Kočkou Lunou pohybujete klasicky za pomocí tří kláves. Ostatní klávesy pak slouží pro uživatelské rozhraní, mimo „ing-game“ režim, například pauza či menu.
Pohyb
\item[W] výskok nahoru
\item[A] směr pohybu doleva
\item[D] směr pohybu doprava
Uživatelské rozhraní
\item[P] pauza
\item[ESC] ukončení hry
3.4.	Úkol hry
Úkolem hry je projít a zdolat různé překážky, zapeklitou mapu až na konec úrovně. Ve hře je celkem pět úrovní. Obtížnost úrovní se postupně zvyšuje, každá další je těžší a těžší.
3.5.	LevelManager
Díky Level Manageru probíhá načítání úrovní z předem vytvořených textových souboru. Tyto soubory můžeme najít ve složce res/levels. Skrze textové soubory můžeme jednotlivý levely libovolně upravovat, a to pouze za pomocí přepisovaní textového dokumentu.
 
Obrázek 5: Textový dokument 2 levelu
\item[B] Značí blok: Je 108x108 pixelu velký.
\item[L] Značí místo: Při spuštění daného levelu se objeví Luna.
\item[P] Značí bonus: Ten přidá Luně čas (tuk).
\item[C] Značí překážku: Pohovku, která na chvíli přehodí ovládání.
\item[S] Značí překážku: Troubu, která na chvíli zpomalí hráče.
\item[M] Značí překážku: Vysavač, který Luně odebere čas (tuk).
\item[F] Značí misku: Ta funguje jako přechod do dalšího těžšího levelu.
 
4.	Kód
Hra se skládá ze tří balíčků (core, core.objects, core.state), do kterých je rozděleno 13 tříd (StartGameLuna, Background, Block, Bonus, Button, Finish, Player, DeadLuna, GameLuna, IntroLuna, LevelsLuna, MenuLuna, PauseLuna).
4.1.	StartGameLuna
Při spuštění programu se spustí hlavní třída startGameLuna. Ta inicializuje okno hry a také všechny stavy. Nastavují se zde také klávesové zkratky (např. P – jako pauza), které platí v celé hře. startGameLuna je potomkem StateBasedGame z knihovny Slick.
 
Obrázek 6: Vytvoření, nastavení a spuštění herního kontejneru v metodě main v tříde StartGameLuna
4.2.	GameLuna
Spustí se funkce init a podle hodnoty levelu (1-5) se spustí funkce načti a zpracuj level, které, nastaví entitám (objektům) jejich vlastnosti a následně je funkce přidej entity vloží to hry. Díky tomu můžeme mít různé překážky v různých levelech.
V metodě update je několik podmínek (zmáčknutí tlačítek pro pauzu či exit, přesun do další úrovně nebo do deadLuna).
Metody setLevel levelUp slouží k změně nastavení nebo nastavení levelu.
Metoda deleteEntities vyčistí hru od pozůstatků z minulé úrovně.
Také se zde nachází funkce getGround která má za úkol vygenerovat pole bloků, což nám poté slouží jako zem.
 
Obrázek 7: Konstruktor a metoda třídy init
4.3.	DeadLuna
DeadLuna náležící do balíčku core.state je stav, který má za úkol informovat hráče o tom, že mu během hry došel tuk (čas) a Luna zhubla na takovou hranici, že zemřela.
Poté co Luna zemře, objeví se mu pozadí deadLuna, na kterém je vyobrazený duch Luny co stoupá do nebe s textem „Now your spirit goes to cookie haven.“. Stisknutím klávesy enter se může vrátit zpět do menu a zkusit to znovu.
 
Obrázek 8: Konstruktor a metoda init GameLuna
4.4.	IntroLuna
Intro slouží jako vstup do hry a obsahuje krátkou animaci. Po uběhnutí se animace zastaví a klávesou enter se přesuneme do menu.
 
Obrázek 9: Metoda init a update třídy InroLuna
4.5.	LevelsLuna
LevelsLuna přesouvají hráče do určitého levelu. Na začátku jsou všechny levely až na 1. zamčeny a postupně se při průchodu hrou odemykají.
Při vstupu do levelu se zavolá metoda deleteEntities ve třídě gameLuna, pro vyčištění levelu. Následně se gameLuna inicializuje funkcí init a dojde ke vstupu do hry.
4.6.	MenuLuna
Jedná se o třídu náležící do balíčku core.state. Jedná se o stav, kde máme na výběr 3 možnosti. První je kliknout na tlačítko hra, které nás přesune rovnou do hry. Druhé je tlačítko levely a to nás přesune na výběr levelu můžeme se vybrat jakýkoliv odemčeny level, který chceme. Třetí je konec a to jednoduše ukončí hru.
 
Obrázek 10: Metoda init třídy MenuLuna
4.7.	PauseLuna
Pauza funguje pouze přímo při hraní. Do pauzy se dostaneme z gameLuna.java při stisknutí klávesy P. Tento stav slouží k tomu, aby si hráč mohl odpočinout či vykonat nějakou činnost mimo hru či počítač. Pauzu samotnou vykonává funkce setPause.
 
Obrázek 11: Metoda update třídy PauseLuna
4.8.	Background
Background neboli pozadí je prvek (objekt), který se objevuje v každém stavu, krom intra (kde je nahrazen animaci). Jeho úkolem je vzít obrázek a umístit ho do stavu.
Ve své hře používám dvě možnosti pozadí. Jedno je statické a druhé je pohyblivé. Statické pozadí najdeme všude kromě gameLuna.
Pohyblivé se vyznačuje s tím, že počítá s hráčem. Při pohybu hráče se pozadí pohybuje v opačném směru, což při kombinaci více pozadí tvoří dojem tzv., paralaxního posunu.
 
Obrázek 12: Metoda update třídy Background
4.9.	Block
Blok je prvek v gameLuna, jehož úkolem je dávat hráčovi  možnost dostat se do cíle. Stejně jako pozadí se i blok paralaxně posouvá o nějakou hodnotu. Princip bloku ve 4 kolizních oblastech, které při kolizi s hráčem ovlivňují jeho vlastnosti jako je rychlost, gravitace atd.
 
Obrázek 13: Vytvoření a nastavení kolizních bloků a hráče v třídě Block
4.10.	Bonus
Bonus má za úkol měnit průběh levelu tím, že pokud hráč koliduje s bonusem, tak nějakým způsobem změní vlastnosti hráče (zpomalí ho, přidá mu tuk, prohodí mu ovládání, odebere mu tuk) a následně se odstraní.
 
Obrázek 14: Metoda update třídy Bonus
4.11.	Button
Button neboli tlačítko. Obsahuje několik funkcí, které kontrolují pozici kurzoru vzhledem k tělu tlačítka.
Konstruktor také obsahuje 2 obrázky. Jeden se objevuje v klidovém stavu a druhý pouze po najetí myši.
 
Obrázek 15: Funkce isMouseClicked
4.12.	Player
Player je třída, která má za úkol spravovat logiku a grafiku skákající tlusté kočky. Krom update a renderu obsahuje také metody define Control, move, moveJump, metody pro interakci s bonusy (plus/minus tuk, změna ovládaní, ...) a také gety a sety, které se využívají v OOP jako nástroje pro získání a úpravu hodnot proměnných.
V konstruktoru se nastaví hodnoty pro tuk, spritesheeet pohybu a 2 animace (doleva a doprava).
Update spouští metody denineControl a move, podle směru mění animaci či spouští časovač při sebrání bonusu zpomalení.
Render vykresluje animaci a panel tuku.
DefineControl obsahuje přiřazení textového řetězce k nějakému vstupu z klávesnice. Také ošetřuje záměnu ovládání při sebrání příslušného bonusu.
Metoda move spolupracuje s definovanými vstupy z metody defineControl a podle stisku klávesy posouvá s kočkou doleva či doprava nebo skáče. Obsahuje také časovače pro skok a pád kočky.
Při skoku či pádu se pracuje s časovačem, který za pomocí vzorce, který využívá goniometrické funkce, mění ypsilonovou souřadnici Luny.
 
Obrázek 16: Část metody move v tříde Player
4.13.	Finish
Finis v podobě misky funguje na principu, že pokud dojde k interakci s miskou provede se stejná posloupnost jako při přechodu z levelů do hry, jen s tím rozdílem že místo funkce setLevel se provede funkce levelUp.
 
Obrázek 17: Konstruktor a update třídy Finish
 
5.	Závěr
Zadáním mé práce bylo vytvořit 2D arkádovou hru v programovacím jazyce Java s určitými požadavky (například pět levelů, které se postupně odemykají, bonusové předměty, tukový časovač a další). Všechny tyto požadavky jsem z mého názoru úspěšně splnila.
Vytvoření této hry pro mě byla nenahraditelná zkušenost jak z grafické, tak kódové stránky. Díky této zkušenosti jsem si navíc rozšířila obzory i z herního světa, nyní mám základní přehled o fungování a hratelnosti moderních her. Svého rozhodnutí tedy nelituji a jsem ráda, že jsem si zvolila právě hru.
V budoucnosti plánuji hru ještě vylepšovat a aktualizovat. V plánu mám minimálně přidat nová prostředí, jednotky a řadu dalších vizuálních doplňků. K plnohodnotné hře dnešních poměrů bych však potřebovala přidat mnohem více než pouze ty výše zmíněné. Konkurenceschopná hra by měla mít určitě autentické hudební pozadí, přizpůsobení všem rozlišením, možnost hráčské volby (například více konců) a samozřejmě větší interakce se hrou. Možností je více a rozhodně některé z nich do hry přidám.
V úvahu připadá i možnost portu na mobilní zařízení. Podobné hry jako Luna’s Adnventure se na chytrých telefonech těší velké oblibě. Tudíž pokud hra dostala všechny plánované vylepšení a úpravy, chtěla bych se o to podělit i s veřejností, tudíž na telefonech.
Práce na Luna’s Adnventure také ve mně vzbudila zájem o tvorbu počítačových her. Do budoucna určitě plánuji vytvořit i jiné herní tituly. Chtěla bych si vytvořit hry i jiných žánrů, abych zjistila interní rozdíly mezi jednotlivými projekty a mohla bych je následně porovnat, respektive přiučit se z toho. Šlo by především o hry s trojrozměrnou grafikou anebo o voxelové hry. 3D grafika nebo náhodně generované prostředí je zcela něco jiného než 2D arkáda, proto bych chtěla prozkoumat i takové žánry.






