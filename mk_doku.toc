\select@language {czech}
\contentsline {chapter}{\IeC {\'U}vod}{6}{chapter*.5}
\contentsline {chapter}{\numberline {1}Pou\IeC {\v z}it\IeC {\'e} technologie}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Java}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}LWGLJ}{7}{section.1.2}
\contentsline {section}{\numberline {1.3}Slick2D}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}StateBaseGame}{8}{subsection.1.3.1}
\contentsline {section}{\numberline {1.4}MarteEngine}{8}{section.1.4}
\contentsline {section}{\numberline {1.5}Entity}{8}{section.1.5}
\contentsline {section}{\numberline {1.6}NetBeans IDE}{8}{section.1.6}
\contentsline {section}{\numberline {1.7}Pr\IeC {\'a}ce s~akvarely}{9}{section.1.7}
\contentsline {section}{\numberline {1.8}BitmapCombine}{10}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Spritesheet}{10}{subsection.1.8.1}
\contentsline {chapter}{\numberline {2}O h\IeC {\v r}e}{12}{chapter.2}
\contentsline {section}{\numberline {2.1}Ko\IeC {\v c}ka}{13}{section.2.1}
\contentsline {section}{\numberline {2.2}Intro}{13}{section.2.2}
\contentsline {section}{\numberline {2.3}Ovl\IeC {\'a}d\IeC {\'a}n\IeC {\'\i } hry}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}\IeC {\'U}kol hry}{16}{section.2.4}
\contentsline {section}{\numberline {2.5}LevelManager}{16}{section.2.5}
\contentsline {section}{\numberline {2.6}Kolizn\IeC {\'\i } syst\IeC {\'e}m}{17}{section.2.6}
\contentsline {chapter}{\numberline {3}K\IeC {\'o}d}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Bal\IeC {\'\i }\IeC {\v c}ek core}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}StartGameLuna}{19}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}Bal\IeC {\'\i }\IeC {\v c}ek core.states}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}GameLuna}{19}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}DeadLuna}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}IntroLuna}{21}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}LevelsLuna}{21}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}MenuLuna}{22}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}PauseLuna}{22}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}CongratulationLuna}{22}{subsection.3.2.7}
\contentsline {section}{\numberline {3.3}Bal\IeC {\'\i }\IeC {\v c}ek core.objects}{22}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Background}{23}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Block}{23}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Player}{23}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Finish}{24}{subsection.3.3.4}
\contentsline {chapter}{\numberline {4}Z\IeC {\'a}v\IeC {\v e}r}{25}{chapter.4}
\contentsline {chapter}{\numberline {A}Seznam obr\'azk\r {u}}{26}{appendix.A}
\contentsline {chapter}{\numberline {B}Zdroje}{27}{appendix.B}
